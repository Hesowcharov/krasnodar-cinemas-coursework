﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.CinemaUsers
{
    public interface ISqlUser
    {
        string UserName { get; }

        string ServerName { get; }

        string ConnectionString { get; }
    }
}
