﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.CinemaUsers
{
    public class SecuredUser : GuestUser
    {
        public string Password { get; }

        public SecuredUser(string serverName, string userName, string password) :
            base(serverName, userName)
        {
            Password = password;
            ConnectionString = string.Format(
                "Data Source={0};uid={1};pwd={2};database=KrasnodarCinemasDB",
                serverName,
                userName,
                password
            );
        }
    }
}
