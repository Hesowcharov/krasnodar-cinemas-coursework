﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.CinemaUsers
{
    public class GuestUser : ISqlUser
    {
        public string UserName { get; }

        public string ServerName { get; }

        virtual public string ConnectionString { get; internal set; }

        public GuestUser(string serverName, string userName)
        {
            ServerName = serverName;
            UserName = userName;
            ConnectionString = string.Format(
                "Data Source={0};uid={1};database=KrasnodarCinemasDB",
                serverName,
                userName
            );
        }
    }
}
