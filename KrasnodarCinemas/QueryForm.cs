﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace KrasnodarCinemas
{
    public partial class QueryForm : Form
    {
        public string Query { get; private set; }
        DBController dbController;
        List<string> tables;
        Dictionary<string, string> dbSchema;

        public QueryForm(DBController controller)
        {
            InitializeComponent();
            dbController = controller;
            this.tables = DBController.engTableNames;
            dbSchema = new Dictionary<string, string>();
            foreach (var name in tables)
            {
                TablesCB.Items.Add(name);
            }
            dbSchema = dbController.GetDBSchema();
        }

        

        private void QueryB_Click(object sender, EventArgs e)
        {
            Query = this.QueryTextBox.Text.Trim();
            if (Query != "")
            {
                this.DialogResult = DialogResult.OK;
            }
            else
            {
                this.DialogResult = DialogResult.Cancel;
            }
            
        }

        private void CancelB_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void TablesCB_SelectedValueChanged(object sender, EventArgs e)
        {
            string chosenTable = (string)TablesCB.SelectedItem;
            ColumnsRTB.Text = dbSchema[chosenTable];
        }
    }
}
