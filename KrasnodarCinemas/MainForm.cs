﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using KrasnodarCinemas.Models;

namespace KrasnodarCinemas
{
    public partial class MainForm : Form
    {
        static readonly List<object> EMPTY = new List<object>();

        DBController dbController;
        List<Movie> movies;
        List<Cinema> cinemas;
        List<Cinema> availableCinemas;
        List<Show> availableShows;
        bool clientMode;
        
        public MainForm(DBController controller)
        {
            InitializeComponent();
            dbController = controller;
            clientMode = dbController.User.UserName == "client";
            InitFields();
            RefreshApplicationControls();
            TablesCB.DataSource = DBController.rusTableNames;
        }

        private void RefreshApplicationControls()
        {
            MovieLB.DataSource = movies.Select(s => string.Format("{0}: {1}", s.ID, s.Name)).ToList();
        }

        private void SetCinemasCLB(IEnumerable<Cinema> cinemas)
        {
            var formattedCinemas = cinemas.Select(s => string.Format("{0}: {1}", s.Name, s.Address));
            CinemasCLB.Items.Clear();
            foreach (var cinema in formattedCinemas)
            {
                CinemasCLB.Items.Add(cinema, false);
            }
        }

        private void InitFields()
        {
            
            movies = dbController.GetMovies();
            cinemas = dbController.GetCinemas();
        }

        private void RefreshDataGridView(DataSet ds, string table)
        {
            this.dataGridView1.DataSource = ds;
            this.dataGridView1.DataMember = table;
        }

        public void ShowTable(string table)
        {
            dbController.UpdateDataSetByQuery(table, "select * from " + table);
            var dataSet = dbController.DataSet;
            RefreshDataGridView(dataSet, table);
            UpdateButton.Enabled = true;
        }

        public void ShowQueryResult(string table, string query)
        {
            try
            {
                dbController.UpdateDataSetByQuery(table, query);
                var dataSet = dbController.DataSet;
                RefreshDataGridView(dataSet, table);
                UpdateButton.Enabled = false;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            dbController.Connection.Close();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            var dataAdapter = dbController.DataAdapter;
            SqlCommandBuilder localSQB = new SqlCommandBuilder(dataAdapter);
            localSQB.ConflictOption = System.Data.ConflictOption.OverwriteChanges;
            dataAdapter.DeleteCommand = localSQB.GetDeleteCommand();
            dataAdapter.UpdateCommand = localSQB.GetUpdateCommand();
            dataAdapter.InsertCommand = localSQB.GetInsertCommand();
            var dt = dbController.DataSet.GetChanges().Tables[0];
            if (dt != null)
            {
                try
                {
                    dataAdapter.Update(dt);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void ComplexQueryB_Click(object sender, EventArgs e)
        {
            QueryForm qform = new QueryForm(dbController);
            var result = qform.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    string rawQuery = qform.Query;
                    int afterSpace = rawQuery.IndexOf("from");
                    string tableName = rawQuery.Substring(afterSpace + 4).Trim().Split(' ')[0];
                    ShowQueryResult(tableName, rawQuery);
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message);
                }
            }
        }

        private void SelectTableB_Click(object sender, EventArgs e)
        {
            var selectedTable = TablesCB.SelectedItem.ToString();
            var engTableName = DBController.tableTranslations[selectedTable];
            ShowTable(engTableName);
        }

        private Movie GetSelectedMovie()
        {
            return movies.ElementAt(MovieLB.SelectedIndex);
        }

        private List<Cinema> GetSelectedCinemas()
        {
            var selectedCinemas = new List<Cinema>();
            var iter = CinemasCLB.CheckedIndices.GetEnumerator();
            while (iter.MoveNext())
            {
                selectedCinemas.Add(availableCinemas.ElementAt(int.Parse(iter.Current.ToString())));
            }
            return selectedCinemas;
        }

        private void MovieLB_SelectedValueChanged(object sender, EventArgs e)
        {
            ClearSelection();
            var selectedMovie = GetSelectedMovie();

            var genres = dbController.GetMovieGenres(selectedMovie);
            var countries = dbController.GetMovieCountries(selectedMovie);
            var filmUnits = dbController.GetFilmUnits(selectedMovie);
            string premiereDate = selectedMovie.PremiereDate;
            var data = new Dictionary<string, string>
            {
                { "Название", selectedMovie.Name},
                { "Оригинальное название" , selectedMovie.OriginalName },
                { "Жанры", string.Join(", ", genres) },
                { "Страна", string.Join(", ", countries) },
                { "Дата премьеры", selectedMovie.PremiereDate },
                { "Бюджет", selectedMovie.Budget },
                { "Продолжительность", selectedMovie.Length },
                { "Слоган", selectedMovie.Slogan },
                { "Описание", selectedMovie.Description },
                { "Съемочный состав фильма","\n" + string.Join(",\n", filmUnits) },
            };
            var filteredData = data.Where(tuple => tuple.Value.Trim() != "");
            var zippedData = filteredData.Select(pair => string.Format("{0}: {1}", pair.Key, pair.Value));            
            string info = string.Join(";\r\n", zippedData);
            MovieInfoRTB.Text = info;

            var availableCinemaIDs = dbController.GetAvailableCinemaIDs(selectedMovie);
            availableCinemas = cinemas.Where(c => availableCinemaIDs.Contains(c.ID)).ToList();
            SetCinemasCLB(availableCinemas);
        }

        private void ClearSelection()
        {
            CinemasCLB.Items.Clear();
            DatesLB.DataSource = EMPTY;
        }

        private void CinemasCLB_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                var selectedMovie = GetSelectedMovie();
                var selectedCinemas = GetSelectedCinemas();
                if (selectedCinemas.Count == 0) { return; }
                availableShows = dbController.GetShowDates(selectedMovie, selectedCinemas);
                var formattedShows = availableShows.Select(s => 
                {
                    var currentCinema = selectedCinemas.Find(c => c.ID == s.CinemaID);
                    return $"{currentCinema.Name}: {s.Date}";
                });
                DatesLB.DataSource = formattedShows.ToList();
            }));
            
        }

        private void DatesLB_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = DatesLB.SelectedIndex;
            if (index == -1) { return; }

            var show = availableShows.ElementAt(index);
            var cinema = availableCinemas.Find(c => c.ID == show.CinemaID);
            var movie = movies.ElementAt(MovieLB.SelectedIndex);
            var availableTickets = dbController.GetAvailableTicketPlaces(show);
            var form = new ReservingTicketForm(availableTickets, show, cinema);
            var dialogResult = form.ShowDialog();

            if (dialogResult == DialogResult.Cancel)
            { return; }

            var reservedTickets = form.SelectedTickets;
            dbController.ReserveTickets(reservedTickets, show);

            var info = new List<string>
            {
                "Статус бронирования",
                $"Фильм: { movie.Name}",
                $"Прололжительность: {movie.Length} м.",
                $"Кинотеатр: {cinema.Name}",
                $"Адрес: {cinema.Address}",
                $"Дата сеанса: {show.Date}",
                $"Цена 1 билета: {show.Price}р.",
                $"Телефон: {cinema.BookingPhone}",
                "=================================",
                $"Забронированные места: {string.Join(", ", reservedTickets)}",
                $"Общая стоимость: {show.Price * reservedTickets.Count}"
            };
            string output = string.Join(";\n", info);
            System.IO.File.WriteAllText(form.SavePath, output);
            MessageBox.Show("Места успешно забронированы!", "Статус бронирования", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (clientMode)
            {
                MessageBox.Show("Нет прав доступа!", "Отказ", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                tabControl.SelectedIndex = 1;
                e.Cancel = true;
                return;
            }
            InitFields();
            RefreshApplicationControls();
            ClearSelection();
        }

        private void CinemaReportButton_Click(object sender, EventArgs e)
        {
            var sfd = new SaveFileDialog();
            sfd.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            if (sfd.ShowDialog() != DialogResult.OK)
            { return; }
            var savePath = sfd.FileName;

            var reports = new List<string>();
            foreach ( var cinema in cinemas )
            {
                string showCountSelect = $"SELECT COUNT(*) FROM Show WHERE CinemaID = {cinema.ID}";
                dbController.UpdateDataSetByQuery("Show", showCountSelect);
                int shows = dbController.DataSet.Tables[0]
                    .AsEnumerable()
                    .Select(r => int.Parse(r[0].ToString()))
                    .ElementAt(0);

                string ticketCountSelect = $"SELECT COUNT(*) FROM Ticket t JOIN Show s ON t.ShowID = s.ID WHERE s.CinemaID = {cinema.ID}";
                dbController.UpdateDataSetByQuery("Ticket", ticketCountSelect);
                int tickets = dbController.DataSet.Tables[0]
                    .AsEnumerable()
                    .Select(r => int.Parse(r[0].ToString()))
                    .ElementAt(0);
                var cinemaReport = new List<string>
                {
                    $"Кинотеатр: {cinema.Name}",
                    $"Адрес: {cinema.Address}",
                    $"Телефон бронирования: {cinema.BookingPhone}",
                    $"Количество сеансов: {shows}",
                    $"Зарезервировано билетов: {tickets}"
                };

                reports.Add(string.Join(";\n", cinemaReport));
            }
            string output = string.Join("\n===========\n", reports);
            System.IO.File.WriteAllText(savePath, output);
            MessageBox.Show(
                "Отчет посещений успешно сгенерирован!", 
                "Статус отчета", 
                MessageBoxButtons.OK, 
                MessageBoxIcon.Information);

        }
    }
}
