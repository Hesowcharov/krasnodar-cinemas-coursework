﻿namespace KrasnodarCinemas
{
    partial class QueryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.QueryTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.QueryB = new System.Windows.Forms.Button();
            this.CancelB = new System.Windows.Forms.Button();
            this.TablesCB = new System.Windows.Forms.ComboBox();
            this.ColumnsRTB = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // QueryTextBox
            // 
            this.QueryTextBox.Location = new System.Drawing.Point(12, 144);
            this.QueryTextBox.Name = "QueryTextBox";
            this.QueryTextBox.Size = new System.Drawing.Size(253, 73);
            this.QueryTextBox.TabIndex = 0;
            this.QueryTextBox.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Текст SELECT запроса";
            // 
            // QueryB
            // 
            this.QueryB.Location = new System.Drawing.Point(190, 223);
            this.QueryB.Name = "QueryB";
            this.QueryB.Size = new System.Drawing.Size(75, 23);
            this.QueryB.TabIndex = 2;
            this.QueryB.Text = "Запрос";
            this.QueryB.UseVisualStyleBackColor = true;
            this.QueryB.Click += new System.EventHandler(this.QueryB_Click);
            // 
            // CancelB
            // 
            this.CancelB.Location = new System.Drawing.Point(109, 223);
            this.CancelB.Name = "CancelB";
            this.CancelB.Size = new System.Drawing.Size(75, 23);
            this.CancelB.TabIndex = 3;
            this.CancelB.Text = "Отмена";
            this.CancelB.UseVisualStyleBackColor = true;
            this.CancelB.Click += new System.EventHandler(this.CancelB_Click);
            // 
            // TablesCB
            // 
            this.TablesCB.FormattingEnabled = true;
            this.TablesCB.Location = new System.Drawing.Point(12, 12);
            this.TablesCB.Name = "TablesCB";
            this.TablesCB.Size = new System.Drawing.Size(89, 21);
            this.TablesCB.TabIndex = 4;
            this.TablesCB.SelectedValueChanged += new System.EventHandler(this.TablesCB_SelectedValueChanged);
            // 
            // ColumnsRTB
            // 
            this.ColumnsRTB.Location = new System.Drawing.Point(109, 12);
            this.ColumnsRTB.Name = "ColumnsRTB";
            this.ColumnsRTB.Size = new System.Drawing.Size(156, 105);
            this.ColumnsRTB.TabIndex = 5;
            this.ColumnsRTB.Text = "";
            // 
            // QueryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(277, 254);
            this.Controls.Add(this.ColumnsRTB);
            this.Controls.Add(this.TablesCB);
            this.Controls.Add(this.CancelB);
            this.Controls.Add(this.QueryB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.QueryTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "QueryForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Произвольный запрос";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox QueryTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button QueryB;
        private System.Windows.Forms.Button CancelB;
        private System.Windows.Forms.ComboBox TablesCB;
        private System.Windows.Forms.RichTextBox ColumnsRTB;
    }
}