﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.Models
{
    public class Cinema
    {
        public int ID { get; }

        public string Name { get; }

        public string Address { get; }

        public string BookingPhone { get; }

        public bool HasOnlineOrder { get; }

        public string StartWorkingTime { get; }

        public string EndWorkingTime { get; }

        public Cinema(
            int id, 
            string name, 
            string address,
            string bookingPhone, 
            bool hasOnlineOrder,
            string startWorkingTime,
            string endWorkingTime)
        {
            ID = id;
            Name = name;
            Address = address;
            BookingPhone = bookingPhone;
            HasOnlineOrder = hasOnlineOrder;
            StartWorkingTime = startWorkingTime;
            EndWorkingTime = endWorkingTime;
        }
    }
}
