﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.Models
{
    public class Movie
    {
        public int ID { get; }

        public string Name { get; }

        public string OriginalName { get; }

        public string Slogan { get; }

        public string PremiereDate { get; }

        public string Budget { get; }

        public string RequiredAge { get; }
        
        public string Length { get; }
        
        public string Description { get; }
         
        public Movie(int id, 
            string name, 
            string originalName, 
            string slogan, 
            string premiereDate, 
            string budget, 
            string requiredAge,
            string length,
            string description)
        {
            ID = id;
            Name = name;
            OriginalName = originalName;
            Slogan = slogan;
            PremiereDate = premiereDate;
            Budget = budget;
            RequiredAge = requiredAge;
            Length = length;
            Description = description;
        }
    }
}
