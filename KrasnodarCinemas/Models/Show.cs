﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KrasnodarCinemas.Models
{
    public class Show
    {
        public int ID { get; }

        public int CinemaID { get; }

        public int MovieID { get; }

        public string Date { get; }
        
        public int Positions { get; }
        
        public int Price { get; } 

        public Show(int id, int cid, int mid, string date, int positions, int price)
        {
            ID = id;
            CinemaID = cid;
            MovieID = mid;
            Date = date;
            Positions = positions;
            Price = price;
        }
    }
}
