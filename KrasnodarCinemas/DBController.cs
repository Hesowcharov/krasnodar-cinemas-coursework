﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using KrasnodarCinemas.Models;
using System.Text.RegularExpressions;

namespace KrasnodarCinemas
{
    public class DBController
    {
        public static List<string> rusTableNames;
        public static List<string> engTableNames;
        public static Dictionary<string, string> tableTranslations;

        static DBController()
        {
            tableTranslations = new Dictionary<string, string> {
                { "Кинотеатры", "Cinema" },
                { "Страны", "Country" },
                { "Роли", "FilmRole" },
                { "Жанр", "Genre" },
                { "Съемочная единица", "FilmUnit" },
                { "Фильм", "Movie" },
                { "Фильм и Жанр", "Movie_Genre" },
                { "Фильм и Страна", "Movie_Country" },
                { "Фильм, Съемочна единица и Роль в фильме", "Movie_FilmUnit_FilmRole" },
                { "Кинонаграды", "FilmAward" },
                { "Номинации", "Nomination" },
                { "Единичная награды", "UnitAward" },
                { "Сеансы показа", "Show" },
                { "Билеты", "Ticket" }
            };
            rusTableNames = tableTranslations.Keys.ToList();
            engTableNames = tableTranslations.Values.ToList();
        }

        public SqlConnection Connection { get; }
        public SqlDataAdapter DataAdapter { get; private set; }
        public DataSet DataSet { get; private set; }
        public CinemaUsers.ISqlUser User { get; }

        public DBController(CinemaUsers.ISqlUser user)
        {
            User = user;
            Connection = new SqlConnection(User.ConnectionString);
        }

        public void UpdateDataSetByQuery(string table, string query)
        {
            DataAdapter = new SqlDataAdapter(query, Connection);
            DataSet = new DataSet();
            DataAdapter.Fill(DataSet, table);
        }

        public List<Movie> GetMovies()
        {
            var table = "Movie";
            UpdateDataSetByQuery(table, "SELECT * FROM " + table);
            var movies = DataSet.Tables[table]
                .AsEnumerable()
                .Select((s) =>
                {
                    var id = int.Parse(s[0].ToString());
                    var name = s[1].ToString();
                    var originaleName = s[2].ToString();
                    var slogan = s[3].ToString();
                    var premiereDate = s[4].ToString();
                    var budget = s[5].ToString();
                    var age = s[6].ToString();
                    var length = s[7].ToString();
                    var desc = s[8].ToString();
                    return new Movie(id, name, originaleName, slogan, premiereDate, budget, age, length, desc);
                }).ToList();
            return movies;
        }

        public List<Cinema> GetCinemas()
        {
            var table = "Cinema";
            UpdateDataSetByQuery(table, "SELECT * FROM " + table);
            var cinemas = DataSet.Tables[table]
                .AsEnumerable()
                .Select(s =>
                {
                    var id = int.Parse(s[0].ToString());
                    var name = s[1].ToString();
                    var address = s[2].ToString();
                    var bookingPhone = s[3].ToString();
                    var hasOnlineOrder = bool.Parse(s[4].ToString());
                    var startWorkTime = s[5].ToString();
                    var endWorkTime = s[6].ToString();
                    return new Cinema(id, name, address, bookingPhone, hasOnlineOrder, startWorkTime, endWorkTime);
                }).ToList();
            return cinemas;
        }

        public EnumerableRowCollection<string> GetMovieGenres(Movie movie)
        {
            string genreTable = tableTranslations["Фильм и Жанр"];
            string genreSelect =
                $"SELECT g.Name FROM {genreTable} mg JOIN Genre g ON mg.GenreID = g.ID " +
                $"WHERE MovieID = {movie.ID}";
            UpdateDataSetByQuery(genreTable, genreSelect);
            var genres = DataSet.Tables[genreTable]
                .AsEnumerable()
                .Select(d => d[0].ToString());
            return genres;
        }

        public EnumerableRowCollection<string> GetMovieCountries(Movie movie)
        {
            string countryTable = tableTranslations["Фильм и Страна"];
            string countrySelect =
                $"SELECT c.Name FROM {countryTable} mc JOIN Country c ON mc.CountryID = c.ID " +
                $"WHERE mc.MovieID = {movie.ID}";
            UpdateDataSetByQuery(countryTable, countrySelect);
            var countries = DataSet.Tables[countryTable]
                .AsEnumerable()
                .Select(r => r[0].ToString());
            return countries;
        }

        public EnumerableRowCollection<string> GetFilmUnits(Movie movie)
        {
            string furTable = tableTranslations["Фильм, Съемочна единица и Роль в фильме"];
            string furSelect =
                $"SELECT fu.FirstName, fu.LastName, fr.Name " +
                $" FROM {furTable} fur JOIN FilmUnit fu ON fu.ID = fur.FilmUnitID" +
                $" JOIN FilmRole fr ON fr.ID = fur.FilmRoleID" +
                $" WHERE fur.MovieID = {movie.ID}";
            UpdateDataSetByQuery(furTable, furSelect);
            var filmUnits = DataSet.Tables[furTable]
                .AsEnumerable()
                .Select(r =>
                {
                    string fullName = r[0].ToString();
                    string secondName = r[1].ToString().Trim();
                    if (secondName != "")
                    {
                        fullName = $"{fullName} {secondName}";
                    }
                    string role = r[2].ToString();
                    return $"{role} - {fullName}";
                });
            return filmUnits;
        }

        public List<Show> GetShowDates(Movie movie, List<Cinema> cinemas)
        {
            string table = tableTranslations["Сеансы показа"];
            string movieCondition = $"MovieID = {movie.ID}";
            var dates = cinemas.Select(c =>
            {
                string cinemaCondition = $"CinemaID = {c.ID}";
                string select =
                 $"SELECT * FROM {table}" +
                 $" WHERE {movieCondition} AND ({cinemaCondition})";
                UpdateDataSetByQuery(table, select);
                var date = DataSet.Tables[table]
                    .AsEnumerable()
                    .Select(r => new Show(
                        int.Parse(r[0].ToString()),
                        int.Parse(r[1].ToString()),
                        int.Parse(r[2].ToString()),
                        r[3].ToString(),
                        int.Parse(r[4].ToString()),
                        (int)double.Parse(r[5].ToString())
                        ));
                return date;
            });
            return dates.Aggregate(new List<Show>(), (acc, el) => { acc.AddRange(el); return acc; });

        }

        public List<int> GetAvailableTicketPlaces(Show show)
        {
            string ticketTable = tableTranslations["Билеты"];
            string positionQuery = $"SELECT Position FROM Ticket WHERE ShowID = {show.ID}";
            UpdateDataSetByQuery(ticketTable, positionQuery);
            var reservedPositions = DataSet.Tables[ticketTable]
                    .AsEnumerable()
                    .Select(r => int.Parse(r[0].ToString()));
            var allPositions = Enumerable.Range(1, show.Positions);
            return allPositions.Where(num => !reservedPositions.Contains(num)).ToList();
        }

        public EnumerableRowCollection<int> GetAvailableCinemaIDs(Movie movie)
        {
            string table = tableTranslations["Сеансы показа"];
            string select =
                $"SELECT c.ID FROM {table} s " +
                $" JOIN Cinema c ON s.CinemaID = c.ID WHERE s.MovieID = {movie.ID}";
            UpdateDataSetByQuery(table, select);
            var cinemaIDs = DataSet.Tables[table]
                .AsEnumerable()
                .Select(r => int.Parse(r[0].ToString()));
            return cinemaIDs;
        }

        public void ReserveTickets(List<int> reservedPositions, Show show)
        {
            string insertQuery = "INSERT INTO TICKET (ShowId, Position) VALUES (@sid, @pos)";
            foreach (int reservedPosition in reservedPositions)
            {
                using (SqlCommand insertCommand = new SqlCommand(insertQuery))
                {
                    insertCommand.Connection = Connection;
                    insertCommand.Parameters.Add("@sid", SqlDbType.Int).Value = show.ID;
                    insertCommand.Parameters.Add("@pos", SqlDbType.SmallInt).Value = reservedPosition;
                    Connection.Open();
                    insertCommand.ExecuteNonQuery();
                    Connection.Close();
                }
            }
        }

        public Dictionary<string, string> GetDBSchema()
        {
            var dbSchema = new Dictionary<string, string>();
            foreach (var name in engTableNames)
            {
                dbSchema.Add(name, string.Empty);
                UpdateDataSetByQuery(name, "select * from " + name);
                string pattern = "xs:element name=\"(?<Name>.+)\" type=\"(?<Type>.+?)\"";
                Regex reg = new Regex(pattern);
                var matches = reg.Matches(DataSet.GetXmlSchema());
                foreach (Match match in matches)
                {
                    dbSchema[name] += string.Format("{0} => {1}\n", match.Groups[1].Value, match.Groups[2].Value);
                }
            }
            return dbSchema;
        }
    }
}
