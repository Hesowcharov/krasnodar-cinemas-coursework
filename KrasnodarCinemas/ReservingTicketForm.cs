﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KrasnodarCinemas.Models;

namespace KrasnodarCinemas
{
    public partial class ReservingTicketForm : Form
    {
        public List<int> SelectedTickets { get; private set; }
        public Show Show { get; }
        public string SavePath { get; private set; }

        public ReservingTicketForm(List<int> availableTickets, Show show, Cinema cinema)
        {
            InitializeComponent();
            Show = show;
            ShowInfoTB.Text = $"{cinema.Name}: {show.Date}";
            PriceTB.Text = show.Price.ToString();
            foreach (var pos in availableTickets)
            {
                TicketsListBox.Items.Add(pos);
            }
        }

        private void ReserveButton_Click(object sender, EventArgs e)
        {
            var tickets = TicketsListBox.CheckedItems;
            if (tickets.Count == 0) { DialogResult = DialogResult.Cancel; return; }

            SelectedTickets = new List<int>();
            foreach(var ticket in tickets)
            {
                int position = int.Parse(ticket.ToString());
                SelectedTickets.Add(position);
            }

            var sfd = new SaveFileDialog();
            sfd.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";
            if (sfd.ShowDialog() != DialogResult.OK )
            { return; }

            SavePath = sfd.FileName;
            this.DialogResult = DialogResult.OK;
        }

        private void ReservingTicketForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(DialogResult != DialogResult.OK)
            {
                DialogResult = DialogResult.Cancel;
            }
            
        }
    }
}
