﻿namespace KrasnodarCinemas
{
    partial class ReservingTicketForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TicketsListBox = new System.Windows.Forms.CheckedListBox();
            this.ReserveButton = new System.Windows.Forms.Button();
            this.ShowInfoTB = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.PriceTB = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // TicketsListBox
            // 
            this.TicketsListBox.FormattingEnabled = true;
            this.TicketsListBox.Location = new System.Drawing.Point(12, 93);
            this.TicketsListBox.Name = "TicketsListBox";
            this.TicketsListBox.Size = new System.Drawing.Size(260, 139);
            this.TicketsListBox.TabIndex = 0;
            // 
            // ReserveButton
            // 
            this.ReserveButton.Location = new System.Drawing.Point(194, 238);
            this.ReserveButton.Name = "ReserveButton";
            this.ReserveButton.Size = new System.Drawing.Size(79, 23);
            this.ReserveButton.TabIndex = 1;
            this.ReserveButton.Text = "Резерв";
            this.ReserveButton.UseVisualStyleBackColor = true;
            this.ReserveButton.Click += new System.EventHandler(this.ReserveButton_Click);
            // 
            // ShowInfoTB
            // 
            this.ShowInfoTB.Location = new System.Drawing.Point(14, 25);
            this.ShowInfoTB.Name = "ShowInfoTB";
            this.ShowInfoTB.ReadOnly = true;
            this.ShowInfoTB.Size = new System.Drawing.Size(259, 20);
            this.ShowInfoTB.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Сеанс";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Цена";
            // 
            // PriceTB
            // 
            this.PriceTB.Location = new System.Drawing.Point(14, 67);
            this.PriceTB.Name = "PriceTB";
            this.PriceTB.ReadOnly = true;
            this.PriceTB.Size = new System.Drawing.Size(77, 20);
            this.PriceTB.TabIndex = 4;
            // 
            // ReservingTicketForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 273);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PriceTB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ShowInfoTB);
            this.Controls.Add(this.ReserveButton);
            this.Controls.Add(this.TicketsListBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ReservingTicketForm";
            this.Text = "Бронирование билета";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ReservingTicketForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox TicketsListBox;
        private System.Windows.Forms.Button ReserveButton;
        private System.Windows.Forms.TextBox ShowInfoTB;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PriceTB;
    }
}