﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KrasnodarCinemas.CinemaUsers;

namespace KrasnodarCinemas
{
    public partial class LoginForm : Form
    {
        Dictionary<string, string> nameToUser = new Dictionary<string, string>
        {
            {"Клиент", "client" },
            { "Эксперт киноиндустрии", "movieEditor" },
            { "Менеджер киносеансов", "showEditor" },
            { "Администратор", "cinemaAdmin" }
        };

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (UserCB.SelectedItem == null)
            { return; }

            string serverName = ServerNameTB.Text.Trim();
            string userName = nameToUser[UserCB.SelectedItem.ToString()];
            string password = PasswordTB.Text;
            if (! PasswordTB.Enabled)
            {
                password = string.Empty;
            }
            
            try
            {
                ISqlUser user;
                if (password == string.Empty)
                {
                    user = new GuestUser(serverName, userName);
                }
                else
                {
                    user = new SecuredUser(serverName, userName, password);
                }
                var dbController = new DBController(user);
                new MainForm(dbController).ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserCB_SelectedValueChanged(object sender, EventArgs e)
        {
            var isClient = UserCB.SelectedItem.ToString() == "Клиент";
            PasswordTB.Enabled = !isClient;
        }
    }
}
